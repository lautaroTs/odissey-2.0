using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersonajeController : MonoBehaviour
{
    [Header ("GameObjects")]
    private Rigidbody rb;
    private Animator anim;
    public GameObject objetoADesactivar;
    private HealthManager healthManager;
    private Timer tiempo;
    public LayerMask groundMask;

    [Header ("Variables")]
    public float speed;
    public float rotacion;
    public float fuerza;
    public float h, v;

    [Header("Mensajes")]
    public Text mensaje1;
    public Text mensaje2;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        transform.Rotate(0, h * Time.deltaTime * rotacion, 0);
        transform.Translate(0, 0, v * Time.deltaTime * speed);
    }

    void Update()
    {
        healthManager = GameObject.Find("HealthManager").GetComponent<HealthManager>();

        if (healthManager.vida > 0)
        {
            objetoADesactivar.SetActive(false);
            moverJugador();
        }
        else if (healthManager.vida == 0)
        {
            mensaje1.text = "HAS MUERTO";
            objetoADesactivar.SetActive(true);
        }
    }

    public void moverJugador()
    {
        h = Input.GetAxis("Horizontal");
        v = Input.GetAxis("Vertical");

        anim.SetFloat("VelX", h);
        anim.SetFloat("VelY", v);

        if (isSalto())
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                anim.SetBool("Saltar", true);
                rb.AddForce(new Vector3(0, fuerza, 0), ForceMode.Impulse);
                Debug.DrawLine(this.transform.position, Vector3.down * 1.5f, Color.red);
            }
            anim.SetBool("TocoSuelo", true);
        }
        else
        {
            estoyCayendo();
        }
    }
    bool isSalto(){
        if(Physics.Raycast(this.transform.position, Vector3.down, 1.5f, groundMask)){
            return true;
        }
        else
        {
            return false;
        }
    }


    public void estoyCayendo()
    {
        anim.SetBool("TocoSuelo", false);
        anim.SetBool("Saltar", false);
    }
}
