using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlteraVida : MonoBehaviour
{
    HealthManager healthManager;
    public int cantidad;
    public float tiempo;
    private float damage;
    private float heal;
    void Awake()
    {
        healthManager = GameObject.FindWithTag("Manager").GetComponent<HealthManager>();
    }

    public void Veneno(){
        damage += Time.deltaTime;
        if(damage > tiempo)
        {
            healthManager.vida -= cantidad;
            damage = 0.0f;
        }
    }
    public void Curacion(){
        heal += Time.deltaTime;
        if(heal > tiempo)
        {
            healthManager.vida += cantidad;
            heal = 0.0f;
        }
    }
    public void Trampas(){
        healthManager.vida -= 30;
    }
    public void ZonaMuerte(){
        healthManager.vida -= 100;
    }
}
