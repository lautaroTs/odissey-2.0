using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControladorDiaNoche : MonoBehaviour
{
    // Luz direccional que vamos a utilizar como nuestro sol.
    [SerializeField]
    private Light sun;

    // El numero de segundos en tiempo real que transcurren en un dia completo en el juego.
    // 86400 para 24 hs reales.
    [SerializeField]
    private float segundosDelDia = 120f;

    // Valor que usamos para calcular el momento actual del dia.
    // 0 (media noche) - 0.25 (amanecer) - 0.5 (medio dia) - 0.75 (atardecer) - 1 (media noche).
    // We define ourself what value the sunrise sunrise should be etc., but I thought these 
    // values fit well. And now much of the script are hardcoded to these values.
    [SerializeField]
    [Range(0, 1)]
    public float tiempoDelDia = 0;

    // Multiplicador del paso del tiempo. Por defecto = 1.
    public float multiplicadorTiempo = 1f;

    // Intencidad inicial del sol.
    float intensidadInicialSol;

    void Start()
    {
        intensidadInicialSol = sun.intensity;
    }

    void Update()
    {
        // Posicion, rotacion e intensidad del sol de acuerdo a la hora del dia.
        UpdateSun();

        // Esto hace que el tiempoDelDia vaya de 0 a 1 respectivamente con los valores especificados anteriormente.
        tiempoDelDia += (Time.deltaTime / segundosDelDia) * multiplicadorTiempo;

        // Si tiempoDelDia es 1, vuelve a 0 para empezar nuevamente en media noche.
        if (tiempoDelDia >= 1)
        {
            tiempoDelDia = 0;
        }
    }

    void UpdateSun()
    {
        // Rota al sol en 360� de acuerdo a la hora del dia.
        // Le saque 90� para que el amanecer empiece en 0.25 y no en 0.
        // La y determina de donde sale y donde se pone el sol.
        // Z no hace nada
        sun.transform.localRotation = Quaternion.Euler((tiempoDelDia * 360f) - 90, 170, 0);

        // Lo siguiente determina la intensidad del sol de acuerdo a la hora del dia.

        // El sol esta en su maxima intensidad durante el dia.
        float multiplicadorIntensidad = 1;
        // Intensidad en 0 entre atardecer y amanecer.
        if (tiempoDelDia <= 0.23f || tiempoDelDia >= 0.75f)
        {
            multiplicadorIntensidad = 0;
        }
        else if (tiempoDelDia <= 0.25f)
        {
            multiplicadorIntensidad = Mathf.Clamp01((tiempoDelDia - 0.23f) * (1 / 0.02f));
        }
        // And fade it out when it sets.
        else if (tiempoDelDia >= 0.73f)
        {
            multiplicadorIntensidad = Mathf.Clamp01(1 - ((tiempoDelDia - 0.73f) * (1 / 0.02f)));
        }

        // Multiply the intensity of the sun according to the time of day.
        sun.intensity = intensidadInicialSol * multiplicadorIntensidad;
    }
}
