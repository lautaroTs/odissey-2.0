using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MensajeInicial : MonoBehaviour
{

    public Text mensajeGenerico;

    void OnTriggerEnter(Collider other){
        if(other.gameObject.CompareTag("Player")){
            mensajeGenerico.text = "Bienvendio a Oddisey";
        }
    }
}
