using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlataformaDesaparece3x3 : MonoBehaviour
{
    public MeshRenderer rend;
    public float time = 3;
    public float time2 = 3;

    void Start(){
        rend = GetComponent<MeshRenderer>();
    }

    void OnTriggerEnter(Collider collider){
        if(collider.gameObject.CompareTag("Player")){
            timer();
        }
    }
    void timer(){
        time -= Time.deltaTime;
        if(time <= 0)
        {
            rend.enabled = false;
            resetTimer();
            timer2();
        }
    }

    void timer2(){
        time2 -= Time.deltaTime;
        if(time2 <= 0)
        {
            rend.enabled = true;
            resetTimer2();
        }
    }

    void resetTimer(){
        time = 3;
    }

    void resetTimer2(){
        time = 3;
    }


}
