using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Carteles : MonoBehaviour
{
    public Text mensajeGenerico;

    void OnTriggerEnter(Collider collider){
        if(collider.gameObject.CompareTag("Player")){
            if(this.name == "Cartel1"){
                mensajeGenerico.text = "Bienvenido a Oddisey";
            }
            if(this.name == "Cartel2"){
                mensajeGenerico.text = "Confiamos en vos ! Buena suerte";
            }
            if(this.name == "Cartel3"){
                mensajeGenerico.text = "DUNGEON";
            }
            if(this.name == "Cartel4"){
                mensajeGenerico.text = "DESIERTO";
            }
            if(this.name == "Cartel5"){
                mensajeGenerico.text = "BOSQUE";;
            }
            if(this.name == "Cartel6"){
                mensajeGenerico.text = "Proximamente !";
            }
            if(this.name == "Cartel7"){
                mensajeGenerico.text = "NIEVE";
            }
        }
    }

    void OnTriggerExit(Collider collider){
        if(collider.gameObject.CompareTag("Player")){
            mensajeGenerico.text = "";
        }
    }
}
