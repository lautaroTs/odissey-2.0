using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Portal : MonoBehaviour
{
    ColeccionablesManager coleccionablesManager;
    public GameObject salirDungeon;

    void Start()
    {
        coleccionablesManager = GameObject.Find("ColeccionablesManager").GetComponent<ColeccionablesManager>();
        salirDungeon.SetActive(false);
    }

    void OnTriggerEnter(Collider collider){
        if(collider.gameObject.CompareTag("Player")){
            if(coleccionablesManager.contador == 0){
                salirDungeon.SetActive(true);
            }
        }
    }
    void OnTriggerExit(Collider collider){
        if(collider.gameObject.CompareTag("Player")){
            salirDungeon.SetActive(false);
        }
    }

    public void dungeonSalir(){
        SceneManager.LoadScene(1);
    }
}
