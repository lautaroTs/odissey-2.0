using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puertas : MonoBehaviour
{
    ColeccionablesManager coleccionablesManager;

    public GameObject puerta1;
    public GameObject puerta2;

    private Vector3 puerta1PosIn = new Vector3(-21.62f, 6, 51.06f);
    private Vector3 puerta2PosIn = new Vector3(-16.57f, 6, 51.06f);

    private Vector3 puerta1PosFin = new Vector3(-27, 6, 51.06f);
    private Vector3 puerta2PosFin = new Vector3(-11, 6, 51.06f);

    private float tiempo = 3;


    void Start()
    {
        coleccionablesManager = GameObject.Find("ColeccionablesManager").GetComponent<ColeccionablesManager>();
    }

    void Update()
    {
        
    }

    void OnTriggerEnter (Collider collider){
        if(collider.gameObject.CompareTag("Player")){
            if(coleccionablesManager.contador <= 12){
                abrirPuertas();
                timer();
            }
        }
    }

    public void abrirPuertas(){
        puerta1.transform.localPosition = puerta1PosFin;
        puerta2.transform.localPosition = puerta2PosFin;
    }

    public void cerrarPuertas(){
        puerta1.transform.localPosition = puerta1PosIn;
        puerta2.transform.localPosition = puerta2PosIn;
    }

    void timer(){
        tiempo -= Time.deltaTime;
        if(tiempo <= 0)
        {
            resetTimer();
            cerrarPuertas();
        }
    }

    void resetTimer()
    {
        tiempo = 3;
    }
}
