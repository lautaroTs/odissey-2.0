using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puertas2 : MonoBehaviour
{
    public GameObject puerta1;
    public GameObject puerta2;

    private Vector3 puerta1PosIn = new Vector3(42.75f, 6, 8.25f);
    private Vector3 puerta2PosIn = new Vector3(42.75f, 6, 3.5f);

    private Vector3 puerta1PosFin = new Vector3(42.75f, 6, 13.25f);
    private Vector3 puerta2PosFin = new Vector3(42.75f, 6, -1.75f);

    private float tiempo = 3;

    void OnTriggerEnter (Collider collider){
        if(collider.gameObject.CompareTag("Player")){
                abrirPuertas();
                timer();
        }
    }

    public void abrirPuertas(){
        puerta1.transform.localPosition = puerta1PosFin;
        puerta2.transform.localPosition = puerta2PosFin;
    }

    public void cerrarPuertas(){
        puerta1.transform.localPosition = puerta1PosIn;
        puerta2.transform.localPosition = puerta2PosIn;
    }

    void timer(){
        tiempo -= Time.deltaTime;
        if(tiempo <= 0)
        {
            resetTimer();
            cerrarPuertas();
        }
    }

    void resetTimer()
    {
        tiempo = 3;
    }
}
