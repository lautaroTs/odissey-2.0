using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataformas : MonoBehaviour
{
    public GameObject target;
    
    void OnTriggerEnter(Collider other){
        if(other.gameObject == target){
            target.transform.parent = transform;
        }
    }
    void OnTriggerExit(Collider other){
        if(other.gameObject == target){
            target.transform.parent = null;
        }
    }
}
