using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cofres : MonoBehaviour
{
    private Animator anim;
    public Text mensajeGenerico;
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        
    }

    void OnTriggerEnter(Collider collider){
        if(collider.gameObject.CompareTag("Player")){
            mensajeGenerico.text = "Presiona J para abrir el cofre";
            if(Input.GetKeyDown(KeyCode.J)){
                anim.SetTrigger("Open");
                if(Input.GetKeyDown(KeyCode.J)){
                    anim.SetTrigger("Close");
                }
            }
        }
    }
    void OnTriggerExit(Collider collider){
        if(collider.gameObject.CompareTag("Player")){
            mensajeGenerico.text = "";
        }
    }
}
