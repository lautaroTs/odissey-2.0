using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ActivadorPuertaDesierto : MonoBehaviour
{
    public GameObject puerta;
    ColeccionablesManager coleccionablesManager;
    public Text mensaje1;
    public UnityEvent<object> abrirPuerta;

    void Start(){
        coleccionablesManager = GameObject.Find("ColeccionablesManager").GetComponent<ColeccionablesManager>();
        puerta.SetActive(true);
    }

    void Update(){
        if(coleccionablesManager.contador <= 12){
            abrirPuerta.Invoke(this);
        }
    }
    public void abrir(){
        puerta.SetActive(false);
        mensaje1.text = "Algo se ha movido ...";
    }
}
