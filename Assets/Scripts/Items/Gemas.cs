using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Gemas : MonoBehaviour
{
    [SerializeField]
    private int numeroGemas;
    public UnityEvent<object> eliminar;

    void OnTriggerEnter(Collider collider){
        if(collider.gameObject.CompareTag("Player")){
            eliminar.Invoke(this);
            //ColeccionablesManager.coleccionablesManager.eliminarGema(numeroGemas);
            Destroy(gameObject);
        }
    }
}
