using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HealthManager : MonoBehaviour
{
    public static HealthManager healthManager;
    public float vida = 100;
    public int porcentajeVida;
    public Image barraDeVida;
    public GameObject barraVida;
    public Text textoVida;

    private void Awake()
    {
        if (healthManager != null & healthManager != this)
        {
            Destroy(this);
        }
        else
        {
            healthManager = this;
        }
    }
    void Update()
    {
        vida = Mathf.Clamp(vida, 0, 100);
        barraDeVida.fillAmount = vida / 100;
        porcentajeVida = (int)vida * 100 / 100;
        textoVida.text = porcentajeVida + "%";
    }

    public void activarBarraVida(){
        barraVida.SetActive(true);
    }

    public void Reset(){
        
    }
}
