using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PortalesManager : MonoBehaviour
{

    public static PortalesManager instance;
    public bool dontDestroyOnLoad;

    public GameObject portalDungeon;
    public GameObject portalDesierto;
    public GameObject portalNieve;
    public GameObject portalBosque;
    
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;

            if (dontDestroyOnLoad)
            {
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }

    void Start(){
        portalDungeon.SetActive(false);
        portalDesierto.SetActive(false);
        portalNieve.SetActive(false);
        portalBosque.SetActive(false);
    }

    void OnTriggerEnter(Collider collider){
        if(collider.gameObject.CompareTag("Player")){
            if(this.name == "PortalDungeon"){
                portalDungeon.SetActive(true);
            }
            if(this.name == "PortalDesierto"){
                portalDesierto.SetActive(true);
            }
            if(this.name == "PortalNieve"){
                portalNieve.SetActive(true);
            }
            if(this.name == "PortalBosque"){
                portalBosque.SetActive(true);
            }
        }
    }

    void OnTriggerExit(Collider collider){
        if(collider.gameObject.CompareTag("Player")){
            portalDungeon.SetActive(false);
            portalDesierto.SetActive(false);
            portalNieve.SetActive(false);
            portalBosque.SetActive(false);
        }
    }

    public void desactivarDungeon(){
        portalDungeon.SetActive(false);
    }
    public void desactivarDesierto(){
        portalDesierto.SetActive(false);
    }
    public void desactivarNieve(){
        portalNieve.SetActive(false);
    }
    public void desactivarBosque(){
        portalBosque.SetActive(false);
    }
}
