using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EscenasManager : MonoBehaviour
{
    public static EscenasManager escenasManager;

    private void Awake()
    {
        if (escenasManager != null & escenasManager != this)
        {
            Destroy(this);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
            escenasManager = this;
        }
    }
}
