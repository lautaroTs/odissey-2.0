using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColeccionablesManager : MonoBehaviour
{
    public static ColeccionablesManager coleccionablesManager;
    public List<GameObject> arrayGemas;
    public Text coleccionables1;
    public int contador;

    private void Awake()
    {
        if (coleccionablesManager != null & coleccionablesManager != this)
        {
            Destroy(this);
        }
        else
        {
            coleccionablesManager = this;
        }
    }

    void Update()
    {
        contador = arrayGemas.Count;
    }

    public void eliminarGema(int elemento){
        arrayGemas.RemoveAt(elemento);
        //contador = arrayGemas.Count;
        coleccionables1.text = "gemas restantes: " + contador;
    }
}
