using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MutanteController : MonoBehaviour
{
    [Header("GameObjects")]
    public Animator anim;
    public Transform objetivo;
    public Transform myTransform;
    public HealthManager healthManager;
    
    [Header("Variables")]
    public float rango;
    public float giro;
    public float velocidad;
    public int rutina;
    public float cronometro;

    void Awake(){
        myTransform = transform;
    }
    void Start()
    {
        anim = GetComponent<Animator>();
        healthManager = GameObject.Find("Manager").GetComponent<HealthManager>();
        objetivo = GameObject.FindWithTag("Player").transform;
    }
    void Update(){
        Comportamiento();
    }

    public void Comportamiento(){
        rango = Vector3.Distance(objetivo.transform.position, transform.position);
        if(rango < 15 && rango > 10){
            anim.SetBool("SawPlayer", true);
            anim.SetBool("OnRange", false);
        }
        if(rango < 10 && rango > 3){
            anim.SetBool("SawPlayer", false);
            anim.SetBool("OnRange", true);
            myTransform.rotation = Quaternion.Slerp(myTransform.rotation, Quaternion.LookRotation(objetivo.position - myTransform.position), giro * Time.deltaTime);
            myTransform.position += myTransform.forward * velocidad * Time.deltaTime;
            Debug.DrawLine(objetivo.transform.position, transform.position, Color.red, Time.deltaTime, false);
        }
        if(rango < 3){
            anim.SetBool("SawPlayer", false);
            anim.SetBool("OnRange", false);
            Ataque();
        }
    }
    public void Ataque(){
        cronometro += 1 * Time.deltaTime;
        if(cronometro >= 3){
            rutina = Random.Range(0, 2);
            cronometro = 0;
        }
        switch(rutina){
            case 0:
                Blade();
                break;
            case 1:
                Punch();
                break;
        }
    }

    public void Blade(){
        anim.SetBool("Blade", true);
        anim.SetBool("Punch", false);
    }
    public void Punch(){
        anim.SetBool("Blade", false);
        anim.SetBool("Punch", true);
    }
    public void Sangrado(){
        float tiempoSangrado = 5;
        if(tiempoSangrado > 0){
            tiempoSangrado -= 1 * Time.deltaTime;
            healthManager.vida -= 5 * Time.deltaTime;
        }
    }
    public void BladeDamage(){
        healthManager.vida -= 20;
        Sangrado();
    }
    public void PunchDamage(){
        healthManager.vida -= 10;
    }
}
