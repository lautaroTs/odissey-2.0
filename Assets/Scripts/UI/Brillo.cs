using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Brillo : MonoBehaviour
{
    public Slider slider;
    public float sliderValue;
    public Image brilloPanel;

    void Start()
    {
        slider.value = PlayerPrefs.GetFloat("brillo", 0.5f);
        brilloPanel.color = new Color(brilloPanel.color.r, brilloPanel.color.g, brilloPanel.color.b, slider.value);
    }

    public void changeSlider(float valor){
        sliderValue = valor;
        PlayerPrefs.SetFloat("brillo", sliderValue);
        brilloPanel.color = new Color(brilloPanel.color.r, brilloPanel.color.g, brilloPanel.color.b, slider.value);
    }
}
