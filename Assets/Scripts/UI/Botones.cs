using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Botones : MonoBehaviour
{
    HealthManager hm;

    void Start(){
        hm = GameObject.Find("HealthManager").GetComponent<HealthManager>();
    }
    public void EscenaInicial(){
        SceneManager.LoadScene(1);
    }

    public void RestartDungeon(){
        SceneManager.LoadScene(2);
        hm.vida = 100;
        
    }
    public void SalirDungeon(){
        SceneManager.LoadScene(1);
        hm.vida = 100;
    }

    public void RestartDesierto(){
        SceneManager.LoadScene(3);
        hm.vida = 100;
    }
    public void SalirDesierto(){
        SceneManager.LoadScene(1);
        hm.vida = 100;
    }
}
