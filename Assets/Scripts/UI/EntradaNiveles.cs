using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EntradaNiveles : MonoBehaviour
{
    public void Dungeon(){
        SceneManager.LoadScene(2);
    }

    public void Desierto(){
        SceneManager.LoadScene(3);
    }

    public void Nieve(){
        SceneManager.LoadScene(4);
    }

    public void Bosque(){
        SceneManager.LoadScene(5);
    }
}
