using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public GameObject objetoADesactivar;

    public int min;
    public int seg;

    public int m;
    public int s;

    [SerializeField]
    private Text timerText;
    public Text mensajes;
    
    void Start()
    {
        startTimer();
    }

    public void startTimer()
    {
        m = min;
        s = seg;
        writeTimer(m, s);
        Invoke("updateTimer", 1f);
    }

    public void stopTimer()
    {

    }

    public void updateTimer()
    {

        s--;
        if (s < 0)
        {
            if(m == 0)
            {
                mensajes.text = "Juego Terminado";
                objetoADesactivar.SetActive(true);
            }
            else
            {
                objetoADesactivar.SetActive(false);
                m--;
                s = 59;
            }
        }
        writeTimer(m, s);
        Invoke("updateTimer", 1f);

    }

    private void writeTimer(int m, int s)
    {

        if(s < 10)
        {
            timerText.text = m.ToString() + ":0" + s.ToString();
        }
        else
        {
            timerText.text = m.ToString() + ":" + s.ToString();
        }

    }
}
